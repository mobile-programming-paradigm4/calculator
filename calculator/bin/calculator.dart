import 'package:calculator/calculator.dart' as calculator;
import 'dart:io';
import 'dart:math';



class Calculator{
  List token = [];
  List posfix = [];
  num evaluate = 0;

  Calculator(String input){
    this.token=(tokenizing(input));
    this.posfix=(infixtoposfix(token));
    this.evaluate=(evaluatepostfix(posfix));
  }

  List tokenizing(String input) {
  List list1 = [];
  String b = "";
  for (var i = 0; i < input.length; i++) {
    if (input[i] == " ") {
      if (b.isNotEmpty) {
        list1.add(b);
        b = "";
      }
    } else {
      b = b + input[i];
    }
  }
  if (!input[input.length - 1].contains(" ")) {
    list1.add(b);
  }
  return list1;
}

List infixtoposfix(List input2) {
  List listoperator = [];
  List listpostfix = [];
  for (int i = 0; i < input2.length; i++) {
    if (input2[i] == "(") {
      listoperator.add(input2[i]);
    } else if (input2[i] == ")") {
      while (listoperator.last != '(') {
        listpostfix.add(listoperator.last);
        listoperator.removeLast();
      }
      listoperator.removeLast();
    } else if (input2[i] == "+" ||
        input2[i] == "-" ||
        input2[i] == "*" ||
        input2[i] == "/" ||
        input2[i] == "^") {
      while (listoperator.isNotEmpty &&
          listoperator.last != '(' &&
          precedence(input2[i]) <= precedence(listoperator.last)) {
        listpostfix.add(listoperator.last);
        listoperator.removeLast();
      }
      listoperator.add(input2[i]);
    } else if (double.tryParse(input2[i]) != null) {
      double.parse(input2[i]) is double;
      listpostfix.add(input2[i]);
    }
  }

  while (listoperator.isNotEmpty) {
    listpostfix.add(listoperator.last);
    listoperator.removeLast();
  }
  return listpostfix;
}

int precedence(String a) {
  if (a == "^") {
    return 3;
  } else if (a == "*" || a == "/") {
    return 2;
  } else if (a == "+" || a == "-") {
    return 1;
  }
  return 0;
}

num evaluatepostfix(List input3) {
  List<num> listvalues = [];
  num right = 0;
  num left = 0;
  num sum = 0;
  for (int i = 0; i < input3.length; i++) {
    if (!"+-*/^".contains(input3[i])) {
      listvalues.add(double.parse(input3[i]));
    } else {
      right = listvalues.last;
      listvalues.removeLast();
      left = listvalues.last;
      listvalues.removeLast();

      if (input3[i] == "+") {
        sum = left + right;
      } else if (input3[i] == "-") {
        sum = left - right;
      } else if (input3[i] == "*") {
        sum = left * right;
      } else if (input3[i] == "/") {
        sum = left / right;
      } else if (input3[i] == "^") {
        sum = pow(left, right);
      }
      listvalues.add(sum);
    }
  }
  return listvalues[0];
}
}

void main() {
  stdout.write("Input : ");
  String cal = stdin.readLineSync()!;
  print(Calculator(cal).evaluate);
}